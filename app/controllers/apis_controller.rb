class ApisController < ApplicationController

  respond_to :html, :json, :xml

  def index
    @apis = Api.all
    respond_with @apis
  end

 def show
    @api = Api.read(params[:id])
    respond_with @api
 end

 def edit
    @api = Api.read(params[:id])
    @filename = params[:id]
 end

 def update
    Api.write(params[:id], params[:api])
    redirect_to "/apis"
 end

end
