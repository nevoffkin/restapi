class Api
 @path = Rails.root + "dbfiles"

  def self.all
    arr_file_names = Array.new
    arr_files = Dir.glob(@path + "*")
    arr_files.each do |files|
      arr_file_names << files.split("/").last
    end
    return arr_file_names
  end

  def self.write(filename, param)
    File.open(@path + filename, 'w') {|f| f.write(YAML.dump(param)) }
  end

  def self.read(filename)
    content = YAML.load(File.read(@path + filename))
  end

end
